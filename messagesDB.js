const fs = require("fs");
const date = new Date();
const path = "./messages";
const messageName = "./messages/" + date.toLocaleTimeString();

let allMessages = [];
const lastFive = [];

module.exports = {
    getMessages () {
        fs.readdir(path, (err, files) => {
            allMessages.length = 0;
            lastFive.length = 0;
            files.forEach(file => {
                fs.readFile(path + "/" + file, (err, data) => {
                    if (err){console.log(err)}
                    allMessages.push(JSON.parse(data));
                });
            });
        });

        if (allMessages.length < 6) {
            return allMessages;
        } else {
            for (let i = allMessages.length - 5; i <= allMessages.length - 1; i++) {
                lastFive.push(allMessages[i]);
            }
            return lastFive;
        }
    },
    addMessage(message) {
        message.id = Date.now();
        message.dateTime = date.toLocaleString();
        fs.writeFileSync(messageName + ".json", JSON.stringify(message));
        return message;
    },
};