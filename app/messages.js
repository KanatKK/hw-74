const router = require("express").Router();
const db = require("../messagesDB");

router.get("/", (req,res) => {
    const messages = db.getMessages();
    res.send(messages);
});
router.post("/", (req,res) => {
    const txt = db.addMessage(req.body);
    res.send(txt);
});

module.exports = router;