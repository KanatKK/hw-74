const express = require("express");
const messages = require('./app/messages');
const app = express();
const db = require("./messagesDB");

db.getMessages();

app.use(express.json());
app.use("/messages", messages);

app.listen(8000, () => {
   console.log("http://localhost:8000");
});